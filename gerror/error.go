package gerror

import (
	"errors"
	"html"
)

// Error struct for wrapping all errors
type Error struct {
	Msg     string `json:"message"` // Public Message
	SCode   int    `json:"-"`       // StatusCode
	DCode   string `json:"code"`    // DotCode
	PrivMsg string `json:"-"`       // Private Message not shown to client
}

func (e *Error) Error() string {
	return html.EscapeString(e.Msg)
}

var (
	// ErrUsernameTaken when a username is taken
	ErrUsernameTaken = errors.New("Username already taken")
	ErrUnauthorized  = errors.New("Unauthorized")

	DCodeServerErr        = "server.error"
	DCodeBadRequest       = "request.bad"
	DCodePathNotFound     = "path.notfound"
	DCodeResourceNotFound = "resource.notfound"
	DCodeConflict         = "conflict.error"
	DCodeUnauthorized     = "unauthorized"
)
