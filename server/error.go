package server

import (
	"fmt"
	"html"
	"net/http"

	"gitlab.com/leonardaustin/aego/gerror"
	"gitlab.com/leonardaustin/aego/log"

	"golang.org/x/net/context"
)

// ErrorWrap used simply to return a wrapped json response
type ErrorWrap struct {
	Error *gerror.Error `json:"error"`
}

// func init() {
// 	notFoundChain := NewChain(MWAddContextAndTrace, MWAddHeaders, MWDoAuth).ThenFunc(NotFoundHandler)
// 	Router.NotFoundHandler = notFoundChain
// }

// Error replies to the request with the specified error message and HTTP code.
// The error message should be plain text.
func Error(ctx context.Context, w http.ResponseWriter, e *gerror.Error) {
	log.Debugf(ctx, "[aago/server.Error] Error: PubMsg: %s, Code: %d, Dot: %s, PrivMsg: %s", e.Msg, e.SCode, e.DCode, e.PrivMsg)

	// HttpCode
	w.WriteHeader(e.SCode)
	w.Header()

	// Unmarshall
	// b, err := json.Marshal(&ErrorWrap{e})
	// if err != nil {
	// 	log.Debugf(ctx, "[aago/server.Error] Unmarshall Error PubMsg: %s, Code: %d, Dot: %s, PrivMsg: %s", e.PubMsg, e.HttpCode, e.DotCode, e.PrivMsg)
	// 	fmt.Fprintln(w, `{"error":{"message":"Internal Server Error","code":"server.error"}}`)
	// 	return
	// }

	fmt.Fprintf(w, fmt.Sprintf(`{"error":{"message":"%s","dotCode":"%s","httpCode":%d}}`, html.EscapeString(e.Msg), e.DCode, e.SCode))
}

// NotFoundHandler returns when route not found
func NotFoundHandler(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	log.Debugf(ctx, "[NotFoundHandler]")
	Error(ctx, w, &gerror.Error{Msg: "Path Not Found", SCode: http.StatusNotFound, DCode: gerror.DCodePathNotFound, PrivMsg: ""})
}
