package server

import (
	"encoding/json"
	"net/http"
	"reflect"
	"strings"
	"time"

	"gitlab.com/leonardaustin/aego/auth"
	"gitlab.com/leonardaustin/aego/auth/domain"
	"gitlab.com/leonardaustin/aego/log"

	"github.com/pborman/uuid"
	"golang.org/x/net/context"
	"google.golang.org/appengine"
)

// MWAddContextAndTrace adds context and trace
func MWAddContextAndTrace(w http.ResponseWriter, req *http.Request) context.Context {
	// Mint Context
	ctx := appengine.NewContext(req)

	// Add trace
	trace := uuid.New()
	if suppliedTrace := req.Header.Get("x-trace-id"); suppliedTrace != "" {
		trace = suppliedTrace
	}
	w.Header().Set("x-trace-id", trace)
	ctx = context.WithValue(ctx, "traceId", trace)
	ctx = context.WithValue(ctx, "requestAcceptedTime", time.Now().UnixNano())

	// log goes last as needs ctx
	log.Debugf(ctx, "[aago/server.MWAddContextAndTrace] Run Complete")
	return ctx
}

// MWAddHeaders adds headers
func MWAddHeaders(ctx context.Context, w http.ResponseWriter, req *http.Request) {
	log.Debugf(ctx, "[aago/server.MWAddHeaders] Run")

	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("content-type", "application/json")
	w.Header().Set("access-control-allow-origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST")
	// w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE")
	// w.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
}

// MWExposeSchema return json of empty struct
func MWExposeSchema(ctx context.Context, endpoint *Endpoint) ([]byte, error) {
	log.Debugf(ctx, "[aago/server.MWExposeSchema] Run")

	type ReqResp struct {
		Request  interface{}
		Response interface{}
	}

	reqRespOb := &ReqResp{
		Request:  reflect.New(reflect.TypeOf(endpoint.Request)).Interface(),
		Response: reflect.New(reflect.TypeOf(endpoint.Response)).Interface(),
	}

	returnStr := []byte{}
	returnStr, err := json.Marshal(reqRespOb)
	if err != nil {
		return returnStr, err
	}

	return returnStr, nil
}

// MWDoAuthAndAddTokenToCtx checks tokens
func MWDoAuthAndAddTokenToCtx(ctx context.Context, req *http.Request, endpoint *Endpoint) (context.Context, bool) {
	log.Debugf(ctx, "[aago/server.MWDoAuth] Run")

	token := &domain.Token{}
	authInfo := strings.TrimSpace(req.Header.Get("Authorization"))
	if authSplit := strings.Split(authInfo, " "); len(authSplit) >= 2 {
		// method := authSplit[0]
		key := authSplit[1]
		token.TokenId = key
	}

	return auth.CheckTokenAndAddToCtx(ctx, token, endpoint.Authorisation)
}
