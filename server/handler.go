package server

// import (
// 	"gitlab.com/leonardaustin/aego/gerror"
//
// 	"golang.org/x/net/context"
// )

// HandlerFunc for the actual handler function
type HandlerFunc interface{}

// type HandlerFunc func(ctx context.Context, req interface{}) (interface{}, *gerror.Error)
