package server

import (
	"gitlab.com/leonardaustin/aego/auth"
)

// Endpoint is a struct with all fields required to make add a new route
type Endpoint struct {
	Name          string // Also used as RPC path
	Path          string
	Handler       HandlerFunc
	Request       interface{}
	Response      interface{}
	SuccessCode   int
	Authorisation auth.Authoriser
}

// AddEndpoint adds a route to the router
func AddEndpoint(r *Endpoint) {

	// Validate
	if r.Name == "" || r.Path == "" {
		return
	}

	// addRestRoutes(r)
	Router.AddRPCEndpoint(r)
	return
}
