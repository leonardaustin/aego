package server

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"sync"

	"github.com/asaskevich/govalidator"
	"golang.org/x/net/context"

	"gitlab.com/leonardaustin/aego/gerror"
	"gitlab.com/leonardaustin/aego/log"
)

var (
	// Router is the default router
	Router = &DefaultRouter{
		Endpoints: map[string]*Endpoint{},
	}
)

// DefaultRouter serves all traffic
type DefaultRouter struct {
	sync.RWMutex

	// Routes is a map of all Routes with key as name
	Endpoints map[string]*Endpoint
}

// AddRPCEndpoint adds rpc path to routes
func (r *DefaultRouter) AddRPCEndpoint(e *Endpoint) {

	// Create path and name from Route.Name
	path := fmt.Sprintf("%s", e.Path)
	// name := fmt.Sprintf("%s_rpc", r.Name)

	r.addToEndpointMap(path, e)
}

func (r *DefaultRouter) addToEndpointMap(path string, e *Endpoint) {
	if _, ok := r.Endpoints[path]; ok {
		panic(fmt.Sprintf("[aago/server.addToRoutesMap] Route already exists! %+v", r))
	}
	r.Lock()
	r.Endpoints[path] = e
	r.Unlock()
}

// ServeHTTP serves http
func (r *DefaultRouter) ServeHTTP(w http.ResponseWriter, req *http.Request) {

	// Mint new context
	ctx := MWAddContextAndTrace(w, req)

	// Add Headers
	MWAddHeaders(ctx, w, req)
	if req.Method == "OPTIONS" {
		w.WriteHeader(200)
		return
	}
	// Can only do JSON Schema validation with POST
	if req.Method != "POST" {
		Error(ctx, w, &gerror.Error{Msg: "Request Body Error", SCode: http.StatusBadRequest, DCode: gerror.DCodeBadRequest, PrivMsg: fmt.Sprintf("[aago/server.MWDoValidate] Read Body Error: %v", "Not a POST request")})
		return
	}

	// Get Body
	defer req.Body.Close()
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		Error(ctx, w, &gerror.Error{Msg: "Request Body Error", SCode: http.StatusBadRequest, DCode: gerror.DCodeBadRequest, PrivMsg: fmt.Sprintf("[aago/server.MWDoValidate] Read Body Error: %v", err)})
		return
	}

	// Match Path
	path := req.URL.Path
	if endpoint, ok := r.Endpoints[path]; ok {
		log.Debugf(ctx, "[aago/server.ServeHTTP] Request Path Found: %v", path)

		// Expose Schema
		if schema := req.URL.Query().Get("schema"); schema == "true" {
			val, err := MWExposeSchema(ctx, endpoint)
			if err != nil {
				Error(ctx, w, &gerror.Error{Msg: "Internal Server Error", SCode: http.StatusInternalServerError, DCode: gerror.DCodeServerErr, PrivMsg: err.Error()})
				return
			}
			fmt.Fprintf(w, "%s", val)
			return
		}

		// Auth and create new context with token
		newCtx, authed := MWDoAuthAndAddTokenToCtx(ctx, req, endpoint)
		if !authed {
			Error(ctx, w, &gerror.Error{Msg: "Unauthorized. Please check the Authorization header e.g. Authorization: Bearer 2738e9ab-3993-4089-ae76-1c33a0e1289a", SCode: http.StatusUnauthorized, DCode: gerror.DCodeUnauthorized, PrivMsg: fmt.Sprintf("Auth failed with token")})
			return
		}
		ctx = newCtx

		jResp, err := r.callHandler(ctx, endpoint, body)
		if err != nil {
			Error(ctx, w, err)
			return
		}

		w.WriteHeader(endpoint.SuccessCode)
		fmt.Fprintf(w, "%s", jResp)
		return
	}

	Error(ctx, w, &gerror.Error{Msg: "Path Not Found", SCode: http.StatusNotFound, DCode: gerror.DCodePathNotFound, PrivMsg: req.URL.String()})
	return
}

func (r *DefaultRouter) callHandler(ctx context.Context, endpoint *Endpoint, body []byte) ([]byte, *gerror.Error) {

	// Request Struct
	reqStruct := reflect.New(reflect.TypeOf(endpoint.Request)).Interface()
	if err := json.Unmarshal(body, &reqStruct); err != nil {
		return nil, &gerror.Error{Msg: "Internal Server Error", SCode: http.StatusInternalServerError, DCode: gerror.DCodeServerErr, PrivMsg: err.Error()}
	}

	_, err := govalidator.ValidateStruct(reqStruct)
	if err != nil {
		log.Debugf(ctx, "[aago/server.callHandler] Validation Run")
		return nil, &gerror.Error{Msg: fmt.Sprintf("Request in not valid. %s", err.Error()), SCode: http.StatusBadRequest, DCode: gerror.DCodeBadRequest, PrivMsg: err.Error()}
	}

	// ctx, req
	inValues := []reflect.Value{reflect.ValueOf(ctx), reflect.ValueOf(reqStruct)}

	// Get Handler Function
	endpointHandler := reflect.ValueOf(endpoint.Handler)
	returnValues := endpointHandler.Call(inValues)
	if err := returnValues[1].IsNil(); !err {
		return nil, returnValues[1].Interface().(*gerror.Error)
	}

	// Marshall response and return
	jResp, err := json.Marshal(returnValues[0].Interface())
	if err != nil {
		return nil, &gerror.Error{Msg: "Internal Server Error", SCode: http.StatusInternalServerError, DCode: gerror.DCodeServerErr, PrivMsg: err.Error()}
	}
	return jResp, nil
}
