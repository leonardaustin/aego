package log

import (
	"fmt"

	"golang.org/x/net/context"
	"google.golang.org/appengine/log"
)

func Criticalf(ctx context.Context, format string, args ...interface{}) {
	log.Criticalf(ctx, addTraceId(ctx, format), args...)
}

func Debugf(ctx context.Context, format string, args ...interface{}) {
	log.Debugf(ctx, addTraceId(ctx, format), args...)
}

func Errorf(ctx context.Context, format string, args ...interface{}) {
	log.Errorf(ctx, addTraceId(ctx, format), args...)
}

func Infof(ctx context.Context, format string, args ...interface{}) {
	log.Infof(ctx, addTraceId(ctx, format), args...)
}

// @todo Think about formating logs into json
func addTraceId(ctx context.Context, format string) string {
	if traceId := ctx.Value("traceId"); traceId != nil {
		format = fmt.Sprintf("[%s] %s", traceId, format)
	}
	return format
}
