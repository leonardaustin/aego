package domain

import "time"

const (
	// TokenTypeUser is "USER"
	TokenTypeUser = "USER"
	// TokenTypeService is "SERVICE"
	TokenTypeService = "SERVICE"
)

// Token struct
type Token struct {
	TokenId          string    `json:"tokenId"` // aka the actual token
	UserId           string    `json:"userId"`
	CreatedTime      time.Time `json:"createdTime"`
	LastActivityTime time.Time `json:"lastActivityTime"`
	ExpirationTTL    int64     `json:"expirationTTL"` // Nanoseconds (yes sucks but a least it is not a float)
	Device           string    `json:"device"`
	TokenRoles       []Role    `json:"tokenRoles"`
	TokenType        string    `json:"tokenType"`
}

// HasRole check whether the token has a role
func (t *Token) HasRole(role Role) bool {
	for _, r := range t.TokenRoles {

		// If checking role within a group
		if role.GroupId != "" {
			return t.HasRoleWithGroup(role)
		}

		// If no group then just check any role
		if r.Id == role.Id {
			return true
		}
	}
	return false
}

// HasRoleWithGroup check whether the token has a role with groupId
func (t *Token) HasRoleWithGroup(role Role) bool {
	for _, r := range t.TokenRoles {
		if r.Id == role.Id && r.GroupId == role.GroupId {
			return true
		}
	}
	return false
}
