package domain

var (
	RoleInternalServiceToService = Role{Id: "internal-service-to-service", Description: "Service2Service for Core"}
	RoleCoreSuperAdmin           = Role{Id: "internal-super-admin", Description: "Core Admin - NOT FOR CLIENTS"}
	RoleClientAdmin              = Role{Id: "client-admin", Description: "Admin role for the group"}
	RoleClientUser               = Role{Id: "client-user", Description: "User within a group"}
	RolePasswordReset            = Role{Id: "password-reset", Description: "Token for password resets"}
	RoleTwoFactor                = Role{Id: "two-factor", Description: "Token use before 2FA is used"}
)

// Role struct
type Role struct {
	Id          string `json:"id"`
	Description string `json:"description"`
	GroupId     string `json:"groupId"`
}

// RoleClientAdminGroup gets a client admin role with group
func RoleClientAdminGroup(groupId string) Role {
	rca := RoleClientAdmin
	rca.GroupId = groupId
	return rca
}

// RoleClientUserGroup gets a client user role with group
func RoleClientUserGroup(groupId string) Role {
	rcu := RoleClientUser
	rcu.GroupId = groupId
	return rcu
}
