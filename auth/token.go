package auth

import (
	"gitlab.com/leonardaustin/aego/auth/domain"
	"gitlab.com/leonardaustin/aego/log"

	"golang.org/x/net/context"
)

var (
	// DefaultTokenReader is the reader the service uses to retreive tokens
	DefaultTokenReader TokenReader
)

// TokenReader reads a token from a datastore or cache
type TokenReader interface {
	TokenRead(ctx context.Context, token *domain.Token) error
}

// CheckTokenAndAddToCtx checks tokens and adds full token struct to context
// Always adds a token (even if missing)
func CheckTokenAndAddToCtx(ctx context.Context, token *domain.Token, authorisation Authoriser) (context.Context, bool) {
	log.Debugf(ctx, "[aago/auth.CheckTokenAndAddToCtx] Run")

	// Always add a token to avoid casting errors in application
	ctx = context.WithValue(ctx, "token", *token)

	// If no token and NoAuthRequired
	if token.TokenId == "" && authorisation.NoAuthRequired {
		log.Debugf(ctx, "[aago/auth.CheckTokenAndAddToCtx] NoAuthRequired")
		return ctx, true
	}

	// @todo - client request to auth service rather than direct to dao
	if err := DefaultTokenReader.TokenRead(ctx, token); err != nil {
		// datastore.ErrNoSuchEntity
		log.Infof(ctx, "[aago/auth.CheckTokenAndAddToCtx] TokenRead Err: %+v", err)
		return ctx, false
	}

	// Add Token to ctx after reading
	ctx = context.WithValue(ctx, "token", *token)
	if authorisation.NoAuthRequired {
		log.Debugf(ctx, "[aago/auth.CheckTokenAndAddToCtx] NoAuthRequired")
		return ctx, true
	}

	err := authorisation.AuthorisationFunction(ctx, token)
	if err != nil {
		log.Infof(ctx, "[aago/auth.CheckTokenAndAddToCtx] AuthorisationFunction Error: %s", err)
		return ctx, false
	}

	if err == nil {
		log.Debugf(ctx, "[aago/auth.CheckTokenAndAddToCtx] AuthorisationFunction OK")
		return ctx, true
	}

	log.Infof(ctx, "[aago/auth.CheckTokenAndAddToCtx] Authorisation All Checks Failed. Token %+v", token)
	return ctx, false
}
