package auth

import (
	"gitlab.com/leonardaustin/aego/auth/domain"
	"gitlab.com/leonardaustin/aego/gerror"
	"gitlab.com/leonardaustin/aego/log"

	"golang.org/x/net/context"
)

// Authoriser struct contains a function that is called during each request
type Authoriser struct {
	NoAuthRequired        bool
	AuthorisationFunction func(ctx context.Context, t *domain.Token) error
}

// RequireOr requires at least one of the given roles to be present in the authorization
func RequireOr(roles ...domain.Role) Authoriser {
	return Authoriser{
		AuthorisationFunction: func(ctx context.Context, t *domain.Token) error {
			log.Debugf(ctx, "[aago/auth.RequireOr] Run")
			for _, role := range roles {
				if t.HasRole(role) {
					return nil
				}
			}
			return gerror.ErrUnauthorized

		},
	}
}

// RequireAnd will requires all the passed roles to be present in the authorization
func RequireAnd(roles ...domain.Role) Authoriser {
	return Authoriser{
		AuthorisationFunction: func(ctx context.Context, t *domain.Token) error {
			log.Debugf(ctx, "[aago/auth.RequireAnd] Run")
			for _, role := range roles {
				if !t.HasRole(role) {
					return gerror.ErrUnauthorized
				}
			}

			return nil
		},
	}
}

// AnyToken will allow anyone throuhg as long as the have a token
func AnyToken() Authoriser {
	return Authoriser{
		AuthorisationFunction: func(ctx context.Context, t *domain.Token) error {
			log.Debugf(ctx, "[aago/auth.AnyToken] Run")
			if t.TokenType == "" {
				return gerror.ErrUnauthorized
			}

			return nil
		},
	}
}

// AnyFullToken will allow anyone with a full token, That is a token that does not have the role for 2FA or PasswordReset
func AnyFullToken() Authoriser {
	return Authoriser{
		AuthorisationFunction: func(ctx context.Context, t *domain.Token) error {
			log.Debugf(ctx, "[aago/auth.AnyFullToken] Run")
			if t.TokenType == "" || t.HasRole(domain.RoleTwoFactor) || t.HasRole(domain.RolePasswordReset) {
				return gerror.ErrUnauthorized
			}

			return nil
		},
	}
}

// OpenToTheWorld will let ANY role through.  Danger Zone.
func OpenToTheWorld() Authoriser {
	return Authoriser{
		AuthorisationFunction: nil,
		NoAuthRequired:        true,
	}
}
