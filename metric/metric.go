package metric

import (
	"time"

	"golang.org/x/net/context"
)

func Inc(ctx context.Context, statName string, value int64) error {
	return nil
}

func Dec(ctx context.Context, statName string, value int64) error {
	return nil
}

func Gauge(ctx context.Context, statName string, value int64) error {
	return nil
}

func GaugeDelta(ctx context.Context, statName string, value int64) error {
	return nil
}

func Timing(ctx context.Context, statName string, value int64) error {
	return nil
}

func TimingDuration(ctx context.Context, statName string, value time.Duration) error {
	return nil
}

func Set(ctx context.Context, statName string, value string) error {
	return nil
}

func SetInt(ctx context.Context, statName string, value int64) error {
	return nil
}

func Raw(ctx context.Context, statName string, value string) error {
	return nil
}
