package client

import (
	"encoding/json"
	"net/http"
	"reflect"

	"gitlab.com/leonardaustin/aego/gerror"
	"gitlab.com/leonardaustin/aego/log"
	"gitlab.com/leonardaustin/aego/server"

	"golang.org/x/net/context"
)

// Call makes a service to service call
// @todo will need to add service2service token
func Call(ctx context.Context, serviceName string, endpointPath string, req interface{}) (interface{}, *gerror.Error) {
	log.Debugf(ctx, "[aago/client.Call] Service: %s, Endpoint: %s", serviceName, endpointPath)

	// Convert to json bytes
	reqCopy := req
	body, err := json.Marshal(reqCopy)
	if err != nil {
		return nil, &gerror.Error{Msg: "Internal Server Error", SCode: http.StatusInternalServerError, DCode: gerror.DCodeServerErr, PrivMsg: "Client Call Path Not Found!"}
	}
	if endpoint, ok := server.Router.Endpoints[endpointPath]; ok {

		// Marshal back into endpoint type
		reqStruct := reflect.New(reflect.TypeOf(endpoint.Request)).Interface()
		if err := json.Unmarshal(body, &reqStruct); err != nil {
			return nil, &gerror.Error{Msg: "Internal Server Error", SCode: http.StatusInternalServerError, DCode: gerror.DCodeServerErr, PrivMsg: err.Error()}
		}

		// ctx, reqCopy
		inValues := []reflect.Value{reflect.ValueOf(ctx), reflect.ValueOf(reqStruct)}

		// Get Handler Function
		endpointHandler := reflect.ValueOf(endpoint.Handler)
		returnValues := endpointHandler.Call(inValues)
		if err := returnValues[1].IsNil(); !err {
			return nil, returnValues[1].Interface().(*gerror.Error)
		}

		return returnValues[0].Interface(), nil
	}

	log.Criticalf(ctx, "[aago/client.Call] Path Not Found with Service: %s, Endpoint: %s", serviceName, endpointPath)
	return nil, &gerror.Error{Msg: "Internal Server Error", SCode: http.StatusInternalServerError, DCode: gerror.DCodeServerErr, PrivMsg: "Client Call Path Not Found!"}
}
